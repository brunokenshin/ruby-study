# Aulas, cursos e exercícios de Ruby
Esse repositório contém os exercícios dos cursos que fiz da linguagem Ruby pela Alura (www.alura.com.br)

##### Curso Ruby I: Introdução a lógica de programação com jogos 
Neste curso, a linguagem ruby é apresentada a partir de um jogo de adivinhação de números.
- Aula 1: Entrada, saída, Strings e números inteiros
- Aula 2: Condicionais, laços, programação imperativa.
- Aula 3: O poder da extração de código
- Aula 4: Arrays, métodos e funções.
- Aula 5: Operações matemáticas, sistemas decimais e hexadecimais
- Aula 6: Case when, while , loop do

##### Curso Ruby II: Continuando seus primeiros passos na programação
Neste curso, continuo meu estudo da linguagem através do desenvolvimento de um jogo da forca.
- Aula 1: Começando o jogo com boas práticas de programação
- Aula 2: Refatorando e organizando o jogo
- Aula 3: Lendo, escrevendo em arquivos e entendendo a pilha de execução

##### Curso Ruby III: Mais lógica de programação em um novo jogo
Neste curso, aperfeiçoarei meus conhecimentos de ruby através de um jogo baseado no Pacman.
- Aula 1: Lidando com Arrays (Foge Foge, um jogo baseado no Pacman)
- Aula 2: Movimentações dos fantasmas e do herói e posições (Botando os fantasmas para correr: arrays associativos, duck typing e outros)
- Aula 3: Os cuidados com arrays usando boas práticas (Matrizes e memória)
- Aula 4: Conceitos de classes e métodos (Estruturas e classes: uma introdução a Orientação a Objetos)
- Aula 5: Usando recursão para solucionar problema de distância (Destruindo os fantasmas: o mundo da recursão)

##### Curso Orientação a Objetos: Melhores técnicas com Ruby

##### Curso Ruby on Rails 4: do zero à web