#Equivalente a definir uma função com 'def' mas podendo ser usada como um objeto é a declaração de um objeto do tipo 'proc':
#Para definir uma função como um objeto (lamda literal) podemos fazer da seguinte forma:
bemvindo = -> (nome) {
	puts "Bem vindo #{nome}"
}

minhafuncao = bemvindo
minhafuncao.call("Urso")


