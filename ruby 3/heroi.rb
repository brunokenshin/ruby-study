class Heroi
	# @linha se refere ao atributo "linha" da classe
	# chamar heroi.linha está invocando o getter para o atributo linha
	
	#"attr_accessor" define getters e setters para os atributos
	#"attr_reader" define apenas os getters para os atributos
	#"attr_writer" define apenas os setters para os atributos

	attr_accessor :linha, :coluna 

	def calcula_nova_posicao(direcao) 
		novo_heroi = dup #self é opcional: heroi = self.dup

		movimentos = {
			"W" => [-1, 0],
			"S" => [+1, 0],
			"A" => [0, -1],
			"D" => [0, +1]
		}

		movimento = movimentos[direcao]
		novo_heroi.linha += movimento[0]
		novo_heroi.coluna += movimento[1]
		novo_heroi
	end

	def direita
		calcula_nova_posicao "D"
	end

	def esquerda
		calcula_nova_posicao "A"
	end

	def cima
		calcula_nova_posicao "W"
	end

	def baixo
		calcula_nova_posicao "S"
	end

	def remove_do(mapa)
		mapa[linha][coluna] = " "
	end

	def coloca_no(mapa)
		mapa[linha][coluna] = "H"
	end

	def to_array
		[linha, coluna]
	end

end